package com.example.OjtTest.Controller;

import com.example.OjtTest.Model.Student;
import com.example.OjtTest.Model.StudentService;
import com.example.OjtTest.Model.Subject;
import com.example.OjtTest.Model.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class MainController {

    private StudentService studentService;

    SubjectService subjectService ;

    public MainController(StudentService studentService, SubjectService subjectService) {
        this.studentService = studentService;
        this.subjectService = subjectService;
    }

    @GetMapping()
    private String index(Model model){
        model.addAttribute("student", studentService.findAll());
        return "index";
    }


    @ResponseBody
    @GetMapping("test/{id}")
    private String indexTest(@PathVariable("id")String id,Model model)
    {

        return id;
    }
    @PostMapping()
    private String storeData(Model model)
    {
        return "index";
    }

    @ResponseBody
    @GetMapping("subject")
    List<Subject> subject(){
        return subjectService.getAll();
    }




    @PostMapping("add")
    private String add(Student student){
        studentService.save(student);
        return "redirect:/";
    }

    @GetMapping("delete/{id}")
    private String delete(@PathVariable("id") Long id){
        studentService.delete(id);
        return "redirect:/";

    }


    @GetMapping("info/{id}")
    private String info(@PathVariable("id") Long id , Model model){
        model.addAttribute("student", studentService.findById(id));
        return "info";
    }

    @PostMapping("update")
    private String update(Student student){
        studentService.update(student);
        return "redirect:/";

    }
}
