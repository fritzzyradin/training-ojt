package com.example.OjtTest.Model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubjectService {
    @Autowired
    SubjectDao subjectDao;

    public List<Subject> getAll() {
        return subjectDao.findAll();
    }
}
