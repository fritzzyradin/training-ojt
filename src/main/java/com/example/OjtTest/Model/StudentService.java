package com.example.OjtTest.Model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class StudentService {

    @Autowired
    private StudentDao studentDao;

    public List<Student> findAll() {
        return studentDao.findAll();
    }

    public void save(Student student) {
        studentDao.save(student);
    }

    public void delete(Long id) {
        studentDao.deleteById(id);
    }

    public Optional<Student> findById(Long id) {
        return studentDao.findById(id);
    }

    public void update(Student student) {
        studentDao.update(student.getFirstName(),student.getLastName(),student.getMiddleName(),student.getId());
    }
}
