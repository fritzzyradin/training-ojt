package com.example.OjtTest.Model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface StudentDao extends JpaRepository<Student,Long> {

    @Modifying
    @Query("UPDATE Student set firstName=:firstName ,lastName=:lastName , middleName=:middleName where id=:id ")
    void update(String firstName, String lastName, String middleName, Long id);
}
