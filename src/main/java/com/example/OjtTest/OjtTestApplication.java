package com.example.OjtTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OjtTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(OjtTestApplication.class, args);
	}

}
